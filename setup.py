import setuptools

# Removing parse_requirements input from pip.req because PIP3 is unstable, and changes from python3 version to version
# https://github.com/AngellusMortis/django_microsoft_auth/pull/405/files
def parse_requirements(filename):
    """ load requirements from a pip requirements file """
    lines = (line.strip() for line in open(filename))
    return [line for line in lines if line and not line.startswith("#")]


with open("README.md", "r") as fh:
    _LONG_DESCRIPTION = fh.read()

_INSTALL_REQS = parse_requirements('requirements.txt')

setuptools.setup(
    name="url_metadata",
    version="0.0.1",
    author="Amen Souissi",
    author_email="amensouissi@ecreall.com",
    description="An URL metadata getter Flask micro website",
    long_description=_LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="https://github.com/assembl/url_metadata",
    packages=setuptools.find_packages(),
    license='AGPLv3',
    setup_requires=['pip>=6'],
    install_requires=_INSTALL_REQS,
    package_data={
        'url_metadata': [
            'templates/*.html',
            'utils/providers.json',
        ]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Operating System :: OS Independent",
    ],
)
